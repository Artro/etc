from urllib import request

from xmltodict import parse
# from pprint import pprint
from datetime import datetime, timedelta
import Sqlite_Handler
import sys
import subprocess as sp

try:
    skip_time = int(sys.argv[1])

    currentTime = datetime.now() - timedelta(minutes = skip_time)
    fs = open('key.dat', 'r')
    key = fs.read()
    url = "http://openapi.airport.co.kr/service/rest/FlightStatusList/getFlightStatusList?" \
          "ServiceKey=" + key +"&" \
          "schAirCode=GMP&" \
          "schIOType=O&" \
          "schLineType=I&" \
          "schStTime=" + currentTime.strftime('%H%M') + "&schEdTime=2400&numOfRows=30"
    req = request.urlopen(url)
    response_body = req.read()
    flight = parse(response_body.decode())

    for row in flight['response']['body']['items']['item']:
          query = "insert or replace " \
                  "into FL (ARP, ST, ET, FLT, ARR_ENG, ARR_KOR, DES_ENG, DES_KOR, GATE, IO, LINE, RMK_ENG, RMK_KOR, DAT) " \
                  "values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',(datetime('now','localtime')))" % \
                  (row.get('airport'),row.get('std'),row.get('etd'),row.get('airFln'),
                   row.get('arrivedEng'),row.get('arrivedKor'),row.get('boardingEng'),row.get('boardingKor'),
                   row.get('gate'),row.get('io'),row.get('line'),row.get('rmkEng'),row.get('rmkKor'))
          Sqlite_Handler.db_execute("Flight.db", query)


    query = "select FLT, ST, ET, ARR_ENG, GATE, LINE, RMK_ENG from FL " \
            "where date(DAT) = date('now','localtime') and ST > '%s' " \
            "order by ST" % currentTime.strftime('%H%M')
    val = Sqlite_Handler.db_select("Flight.db", query)

    fs = open('DEPARTURE.txt', 'w')
    for row in val:
        fl = ''
        for item in row:
            fl += item + "\t"
        fl = fl.replace("None", "    ")
        fl = fl.replace("국제", "I")
        print(fl)
        fs.write("%s\n" % fl)
    fs.close()

except Exception as e:
    print(e.args[0])

finally:
    sp.getstatusoutput("taskkill.exe /f /im FlightInformation.exe")