from cx_Freeze import setup, Executable
import sys
# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [],
                    excludes = [],
                    create_shared_zip = False,
                    append_script_to_exe = True,
                    compressed = True
                    )
print(sys.path + ['xmltodict'])
import sys
base = ''
if sys.platform == 'win32':
    base = 'Console'

executables = [
    Executable('FlightInformation.py',
               base=base,
               targetName = 'FlightInformation.exe',
               icon = 'Main.ico'
               )
]

setup(name='Flight Information',
      version = '1.0.1',
      description = 'Flight Information Ver 1.0.1',
      executables = executables,
      options = dict(build_exe = buildOptions)
      )
