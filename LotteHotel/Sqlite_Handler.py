# -*- coding: utf-8 -*-

import sqlite3

def db_select(file, query):
    db = sqlite3.connect(file)
    cursor = db.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    db.close()
    return result

def db_execute(file, query):
    db = sqlite3.connect(file)
    cursor = db.cursor()
    cursor.execute(query)
    cursor.close()
    db.commit()
    db.close()