# -*- coding: utf-8 -*-

import sys
from PySide.QtGui import *
from PySide.QtCore import *

from Ultrasensor_UI import Ui_Form


class MainWindow(QWidget, Ui_Form):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.pushButton_Exit.clicked.connect(self.exitWindow)
        self.dial.valueChanged.connect(self.spinValue)
        self.spinBox.valueChanged.connect(self.dialValue)

    def spinValue(self):
        self.spinBox.setValue(self.dial.value())
        print(int(self.lcdNumber_Sensor.value()))
        self.lcdNumber_Total.display(1)
    def dialValue(self):
        self.dial.setValue(self.spinBox.value())
    def exitWindow(self):
        exit()

app = QApplication(sys.argv)
# app.setApplicationName('Description')

WindowForm = MainWindow()
# WindowForm.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
# WindowForm.setWindowState(Qt.WindowMaximized)
WindowForm.show()
sys.exit(app.exec_())